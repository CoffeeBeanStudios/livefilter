<?php
	require( '../../../../../wp-load.php' );

	/*
		return a json object in the form of 
		{"panels":[{"src":"img/1.jpg", "title":"Panel Title", "caption":"Panel Caption", "link":"http://www.google.com"}]}
	
	*/
	$page = 1;
	$postsPerPage = 20;
	$category = 'All';
	if(isset($_POST['category']) && (!empty($_POST['category']))) {
		$category = $_POST['category'];
	}
	
	if(isset($_POST['page']) && (!empty($_POST['page']))) {
		$page = $_POST['page'];
	}	
	
	$panels = array();	
	
	// get all of my panels
	global $post;
	$offset = ($page-1) * $postsPerPage;
	if ($category != 'All'){
		$args = array( 'numberposts' => $postsPerPage, 'offset' => $offset, 'orderby' => 'title', 'order'=>'ASC', 'category' => $category );
	}
	else{
		$args = array( 'numberposts' => $postsPerPage, 'offset' => $offset, 'orderby' => 'title', 'order'=>'ASC');
	}
	
	$cnt = 0;
	$myposts = get_posts( $args );
	foreach( $myposts as $post ){
		// get each category
		$cats = '';
		$post_categories = wp_get_post_categories( $post->ID );																	
		foreach($post_categories as $c){
			$cat = get_category( $c );
			$cats = $cats . ' ' . $cat->cat_ID;
		}
	
		setup_postdata($post); 
		
		$panel = array();
		$panel['src'] = grab_first_image();
		$panel['title'] = $post->post_title;
		$panel['caption'] = '';	
		$panel['link'] = get_permalink($post->ID);
		$panel['categories'] = $cats;
		array_push($panels, $panel);
	}
	
	$result = array();
	$result['panels'] = $panels;
	
	print(json_encode($result));	
	
	
	
	function grab_first_image() {
		global $post, $posts;
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		$first_img = $matches[1][0];

		if(empty($first_img)) {
		$first_img = "/path/to/default.png";
		}
		return $first_img;
	}

?>