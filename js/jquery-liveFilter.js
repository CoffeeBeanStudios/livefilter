$(function() {
	var DEBUG = false;
    var version = '1.1';
	var slideshow;
	
    $.fn.liveFilter = function(options) {
        return this.each(function() {
            var $this = $(this);
			
            new liveFilter($this, options);
        });
    };
});

function liveFilter(element, options){
	var self = this;
	var $element = element;
	var currCat = '';
	var jsonPanels = [];
	var ajaxOutOfData=false;
	
	// options
	var ajaxUrl = '';
	var defaultCat = 'all';
	var panelsPerPage = 20;
	var panelsLoaded = 0;
	var currPage = 1;
	
	if (options){
		if ('ajax' in options){
			ajaxUrl = options['ajax'];
			
			if ('defaultCat' in options){
				defaultCat = options['defaultCat'];
			}
		}
	}
	
	self.init = function(){
		// everything is done on category click
		$element.children('.categories').children('.category').click(self.categoryClick);
		
		// load more results if they scroll to the bottom
		$(window).scroll(self.windowScroll);
		
		// if it's ajax load some data
		if (!ajaxUrl){
			self.filter(defaultCat);
		}
		else{
			self.loadAjax(defaultCat);
		}
	};
	
	self.categoryClick = function(){
		var cat = $(this).attr('catid');
			
		if (ajaxUrl==""){
			self.filter(cat);
		}
		else{
			panelsLoaded = 0;
			ajaxOutOfData = false;
			self.loadAjax(cat);
		}
	}
	
	self.windowScroll = function(){
		// see if we need to load more results		
		if   ((window.innerHeight + $(window).scrollTop()) >= $element.height() - $element.position().top){
			self.loadNextPage();
		}
	}
	
	self.loadNextPage = function(){
		if (loadingPanels){return;}
		
		loadingPanels = true;
		currPage++;
		self.loadPanels(panelsLoaded+1, panelsPerPage);
	}
	
	self.loadPanels = function(start, count){
		if (start>=jsonPanels.length){
			if (ajaxOutOfData){
				return
			}
			else{
				self.loadAjax(currCat);
				return;
			}				
		}
	
		// for each image build the dom 
		var out;
		var cnt=0;
		for (var e = start; e < jsonPanels.length; e++){
			var panel = jsonPanels[e];
			out = 
					'	<a class="panel" style="display: none" href="' + panel.link + '">' +
					'		<div class="imageContainer"> ' +
					'			<img class="image" src="' + panel.src + '">' +
					'  		</div>' + 
					'		<div>' +
					'			<span class="title">' + panel.title + '</span><br>' +
					'			<span class="caption">' + panel.caption + '</span>' +
					'		</div>' +
					'	</a>';
			
			$element.children('.panels').append(out);
			
			// break our results at our limit
			cnt++;			
			
			// if it's ajax we want to show everything that's loaded
			if ((ajaxUrl=='')&&(cnt>panelsPerPage)){
				break;
			}
		}
		
		loadingPanels = false;
		panelsLoaded = e;
		$('.panel').show();
	}
	
	self.filter = function(cat){
		// show all panels
		if (cat == defaultCat){
			$element.children(".panels").children(".panel").show('fast');
		}
		// filter to one category
		else{
			$element.children(".panels").children(".panel").each(function(){
				if($(this).hasClass(cat)){
					$(this).show('fast');
				}
				else{
					$(this).hide('fast');
				}
			});
		}
	}
	
	self.loadAjax = function(cat){
		
		currCat = cat;
		
		// clear all of the panels
		//$element.children(".panels").empty();
		
		// show loading image
		
		// request more panels
		$.ajax({
		  url: ajaxUrl,
		  data: {'category': cat,
				 'page': currPage
				},
		  type: 'post',
		  success: self.ajaxLoaded
		});
	}	
	
	self.ajaxLoaded = function(data){
		var json = jQuery.parseJSON(data);	
		if (json.panels.length==0){
			ajaxOutOfData = true;
		}
		jsonPanels = jsonPanels.concat(json.panels);		
		self.loadPanels(panelsLoaded, panelsPerPage);
	}
	
	self.showLoadingBar = function(){
		self.append('<img class="simpleShow_loading" src="img/ajax-loader.gif"/>');
	}
	
	self.hideLoadingBar = function(){
		$('.simpleShow_loading').css('visibility', 'hidden');
	}
	
	self.init();	

	return self;
}