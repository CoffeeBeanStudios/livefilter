<?php
	/*
		return a json object in the form of 
		{"panels":[{"src":"img/1.jpg", "title":"Panel Title", "caption":"Panel Caption", "link":"http://www.google.com"}]}
	*/

	$category = 'all';
	if(isset($_POST['category']) && (!empty($_POST['category']))) {
		$category = $_POST['category'];
	}
	
	$page = 1;
	$postsPerPage = 20;
	if(isset($_POST['page']) && (!empty($_POST['page']))) {
		$page = $_POST['page'];
	}		
	
	$panels = fillArray();
	$offset = ($page-1) * $postsPerPage;
	$cnt = 0;
	$skipped = 0;
	
	$filtered = array();
	foreach($panels as $panel){
		// skip them until we reach what we want
		if ($cnt<$offset){
			$skipped++;
			continue;
		}
		
		if ($panel['category']==$category){
			array_push($filtered, $panel);
		}
		
		$cnt++;		
		// don't return too many
		if ($cnt>$postsPerPage){
			break;
		}
		
	}
	$panels = $filtered;
	
	
	$result = array();
	$result['panels'] = $panels;
	
	print(json_encode($result));	
	
	
	function fillArray(){
		// array of panels for the result
		$panels = array();	
		
		$repeat = 10;
		
		for ($i==0; $i<$repeat; $i++){
			$panel = array();
			$panel['src'] = 'img/dog2.jpg';
			$panel['title'] = 'dog 2';
			$panel['caption'] = 'dog 2';	
			$panel['link'] = 'http://www.google.com';
			$panel['category'] = 'dog';
			array_push($panels, $panel);

			$panel = array();
			$panel['src'] = 'img/cat1.jpg';
			$panel['title'] = 'cat 1';
			$panel['caption'] = 'cat 1';	
			$panel['link'] = 'http://www.google.com';
			$panel['category'] = 'cat';
			array_push($panels, $panel);
			
			
			$panel = array();
			$panel['src'] = 'img/mouse1.jpg';
			$panel['title'] = 'mouse';
			$panel['caption'] = 'mouse';	
			$panel['link'] = 'http://www.google.com';
			$panel['category'] = 'mouse';
			array_push($panels, $panel);	
		}

		return $panels;
	}
?>

