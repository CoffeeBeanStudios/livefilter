simpleShow
=========

A pretty nice little jQuery slideshow.  
  
Example
-
[Here is a good example](http://www.coffeebeanstudios.com/projects/simpleshow/example.html)

Configuration
-
There are some simple options when initializing the slide show.
	- width : width of the entire slide show. Image widths are stretched to this.
	- interval : time interval between slides in milliseconds.
	- showMenu : true or false, whether or not to show the menu on clicking a slide.
	- ajax : url to a source for the content
	
Using it
-
Link the javascript file and the CSS in your head section.

	<script src="js/jquery-simpleShow.js"></script>
	<link href="css/jquery-simpleShow.css" rel="stylesheet">
	
If you're going to use an AJAX datasource, create an empty div with the class "simpleShow" and this:

	<script>
		$(document).ready(function() {
			$('.simpleShow').simpleShow({interval: '4000',
										ajax:'datasource.html'});
		});
	</script>
	
Or if you're going to create your slides in DOM in line you can use 

	<script>
		$(document).ready(function() {
			$('.simpleShow').simpleShow({interval: '4000'});
		});
	</script>

	
	
DOM
-
If you prefer to specify your slides inline use the following format

	<div class="simpleShow_Slide">
		<a href="http://www.slideurl.com">
			<img src="pathtoimage.png">
			<div>
				<span class="simpleShow_title">Slide Title</span><br>
				<span class="simpleShow_caption">Slide Caption</span>
			</div>
		</a>
	</div>

Ajax
-
Ajax datasource is in the following JSON format

	{"slides":[{"src":"img/1.jpg", "title":"Title 1", "caption":"Caption 1", "link":"http://www.google.com"},
	{"src":"img/2.jpg", "title":"Title 2", "caption":"Caption 2", "link":"http://www.google.com"},
	{"src":"img/3.jpg", "title":"Title 3", "caption":"Caption 3", "link":"http://www.google.com"},
	]}
	
Options
-
Below is a call with all options specified

	$('.simpleShow').simpleShow({interval: '4000',
								 showMenu: true,
								 width: 600px,
								 ajax: "datasource.html"});


Requirements
-
	- jQuery

License
-
MIT